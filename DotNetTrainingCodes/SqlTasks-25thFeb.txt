============ADDITION, SUBSTRACTION, MULTIPLICATION, DIVISION PROGRAM=======================


DECLARE @num1 int;
	declare	@num2 int;
SET @num1 = '72'; 
SET	@num2 = '-64';

if(@num2 > 0)
BEGIN
	SELECT @num1 AS First_Number,
@num2 AS Second_Number,
@num1 + @num2 AS Addition,
@num1 - @num2 AS Subtraction,
@num1 * @num2 AS Multiplication, @num1 / @num2 AS Division;
END
else
BEGIN
 SELECT @num1 AS First_Number,
@num2 AS Second_Number,
@num1 + @num2 AS Addition,
@num1 - @num2 AS Subtraction,
@num1 * @num2 AS Multiplication, 'Infinity' AS Division;
END



=====================CONTINUOUS SUBSTRACTION PROGRAM=================================

DECLARE @number1 int;
DECLARE @number2 int;
SET @number1 = '18';
SET @number2 = '3';

if(@number1 > @number2)
BEGIN

WHILE(@number1 > 0)
BEGIN
print @number1
SET @number1 = @number1 - @number2;
END
END






=================GENDER QUERY USING CASE WHEN PROGRAM=================================

DECLARE @GENDER varchar(10);
SELECT * from CompanyClient where Gender = '0' 
set @GENDER = 'Male'

select @GENDER AS NewColumn


SELECT ClientID, ClientName, Contact, City, EmployeeID, Department, Gender,
CASE
WHEN Gender = '0' THEN 'Male'
WHEN Gender = '1' THEN 'Female'
WHEN Gender = '2' THEN 'Other'
ELSE 'Unknown'
END AS Gender 
FROM CompanyClient; 
