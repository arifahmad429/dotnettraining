﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPITask.Models
{
    public class Data
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Department { get; set; }
        public int Id { get; set; }

    }
}
