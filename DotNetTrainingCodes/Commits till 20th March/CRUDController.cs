﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITask.Models;

namespace WebAPITask.Controllers
{
    public class CRUDController : ApiController

    {
        //GET() CRUD Operation

        [HttpGet]
        [ActionName("GetEmployeeByID")]

        public Data Get()
        {
            SqlDataReader reader = null;
            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = @"DATA SOURCE = LAPR259; initial catalog = master; integrated security = true";

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select * from table2 ";
            sqlCmd.Connection = sqlCon;
            sqlCon.Open();
            reader = sqlCmd.ExecuteReader();
            Data detail = null;

            if (reader.Read())
            {

                detail = new Data();
                detail.Id = Convert.ToInt32(reader.GetValue(0).ToString());
                detail.Firstname = reader.GetValue(1).ToString();
                detail.Lastname = reader.GetValue(2).ToString();
                detail.Department = reader.GetValue(3).ToString();
                
            }
            sqlCon.Close();
            return detail;

        }

        //Post() operation
        
        [HttpPost]
        public void AddEmployee(Data employee)
        {

            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = @"DATA SOURCE = LAPR259; initial catalog = master; integrated security = true;";
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandType = CommandType.Text;
            sqlcmd.CommandText = "insert into table2 (id, firstname,lastname, department) values (@id, @firstname,@lastname, @department)";
            sqlcmd.Connection = sqlCon;


            sqlcmd.Parameters.AddWithValue("@id", employee.Id);
            sqlcmd.Parameters.AddWithValue("@firstname", employee.Firstname);
            sqlcmd.Parameters.AddWithValue("@lastname", employee.Lastname);
            sqlcmd.Parameters.AddWithValue("@department", employee.Department);
            sqlCon.Open();
            int rowinserted = sqlcmd.ExecuteNonQuery();

            sqlCon.Close();
        }

        // Delete() CRUD Operation

        [ActionName("DeleteEmployee")]
        public void DeleteEmployeeByID(int id)
        {
            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = @"data source = LAPR259; initial catalog = master; integrated security = true;";

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "delete from table2 where id=" + id + "";
            sqlCmd.Connection = sqlCon;
            sqlCon.Open();
            int rowDeleted = sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
        }

    }
}

