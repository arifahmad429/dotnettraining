﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SqlConnectionss
{
    class AdoNet
    {
        public static void Main(string[] args)
        {


            // Establishing the connection using Constructor

            SqlConnection cmd = new SqlConnection();

            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPR259; Initial Catalog=master; Integrated Security = True;");


            try
            {
                sqlCon.Open();

                Console.WriteLine("=========Connection is Open===========\n\n");


                string query;

                // Calling the Store Procedure along with Parameters

                query = "exec GetClientbyParameters @Gender = '0', @Department = 'Xamarin';";


                SqlDataAdapter command = new SqlDataAdapter(query, sqlCon);
                DataTable table = new DataTable();
                command.Fill(table);

 
                // Fetching the required details

                foreach (DataRow row in table.Rows)
                {
                    Console.WriteLine(row["ClientID"] + " " + row["ClientName"]);
                }


            }


            catch (Exception e)
            {
                Console.WriteLine(e);
                sqlCon.Close();
            }

            Console.ReadKey();
        }
    }
}
 