﻿using System;

namespace InheritanceTask
{


    interface IPainter
    {
        void Paint();
    }

    interface SeatCover
    {
        void ChangeSeat();
    }

    public enum Wheels
    {
        Bike = 2,
        Car = 4
    }
    public class Vehicle : IPainter
    {
        string vehicleName;
        string vehicleColor;
        public int vehicleSpeed;
        int vehicleWheels;
        public readonly int MaxSpeed = 264;

        public Vehicle(string color, int noOfWheels, int maxSpeed)
        {
            vehicleColor = color;
            vehicleWheels = noOfWheels;
            maxSpeed = MaxSpeed;

        }

        public void Paint()
        {
            Console.WriteLine("You can change the color of your vehicle");


        }

        public string VehicleColor
        {
            get
            {
                return vehicleColor;
            }
            set
            {
                vehicleColor = value;
            }
        }


        

        public int VehicleSpeed
        {
            get
            {
                return vehicleSpeed;
            }
            set
            {
                vehicleSpeed = value;
            }
        }

        public string VehicleName
        {
            get
            {
                return vehicleName;
            }
            set
            {
                vehicleName = value;
            }
        }


        sealed class Car : Vehicle
        {

            
            public Car(string vehicleColor, int noOfWheels, int vehicleSpeed) : base("Brown", 4, 132)
            {
                vehicleWheels = (int)Wheels.Car;

                Console.WriteLine("Ferrari of {0} having {1} wheels running at speed {2} Km/hr", vehicleColor, vehicleWheels, vehicleSpeed);
            }

            void CaclulateTollAmount(int rupees)
            {

            }

            public void ChangeSeat()
            {
                Console.WriteLine("You can change the seat of your Car here");
            }
        }

        sealed class Bike : Vehicle
        {

            

            public Bike(string vehicleColor, int noOfWheels, int vehicleSpeed) : base("Black", 3, 85)
            {
                vehicleWheels = (int) Wheels.Bike;

                Console.WriteLine("Yamaha R-15 of {0} having {1} wheels running at speed {2} Km/hr", VehicleColor, vehicleWheels, vehicleSpeed);
            }

            void CalculateTollAmount()
            {

            }

            public void ChangeSeat()
            {
                Console.WriteLine("You can change the seat of your Bike here");
            }
        }


        public class Program
        {
            public static void Main(string[] args)
            {

                Console.WriteLine(" Press a button to Start the CAR..!!");
                Console.ReadKey();
                Car obj = new Car("White", 4, 234);
                obj.ChangeSeat();
                obj.Paint();
                Console.ReadKey();
                Console.WriteLine(" Press a button to Start the BIKE..!!");
                Console.ReadKey();
                Bike obj1 = new Bike("Red", 2, 67);
                obj1.ChangeSeat();
                obj1.Paint();


                
                

                Console.ReadKey();
            }
        }
    }
}

