﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTrainingCodes
{
    class List
    {
        public static void Main(string[] args)
        {
            List<int> a = new List<int>();
            a.Add(32);
            a.Add(97);
            a.Add(12);
            a.Add(65);
            a.Add(12);

            List<int> b = new List<int>();
            b.AddRange(a);


            a.Remove(12);


            int p = 0;

            Console.WriteLine("The elements of First List are:\n");


            foreach (int k in a)
            {
                Console.Write("At Position {0}: ", p);
                Console.WriteLine(k);
                p++;
            }

            p = 0;
            Console.WriteLine("\n\nThe elements of Second List after \ndeleting an element from first list are:\n");

            foreach (int k in b)
            {
                Console.Write("At Position {0}: ", p);
                Console.WriteLine(k);
                p++;
            }

            Console.ReadKey();
        }
    }
}
