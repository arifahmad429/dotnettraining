﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITask.Models;

namespace WebAPITask.Controllers
{
    public class CRUDController : ApiController

    {

        SqlConnection sqlCon = new SqlConnection();
        

        public SqlConnection Connection()
        {
            try
            {
                sqlCon.ConnectionString = @"DATA SOURCE = LAPR259; initial catalog = master; integrated security = true";
            }

            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            return sqlCon;
        }




        //GET() CRUD Operation

        [HttpGet]
        [ActionName("GetAllPersonDetails")]

        public DataSet Get()
        {
            DataSet dataSet = new DataSet();
            sqlCon = Connection();

            try
            {
                sqlCon.Open();
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "Select * from table2";
                sqlCmd.Connection = sqlCon;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
                dataAdapter.Fill(dataSet);

            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            finally
            {
                sqlCon.Close();

            }

            return dataSet;
        }




        //Post() operation

       
        public void AddPerson(Data person)
        {
            sqlCon = Connection();

            try
            {

                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "insert into table2 (id,firstname,lastname,department) values (@id,@firstname,@lastname,@department)";
                sqlCmd.Connection = sqlCon;
                sqlCmd.Parameters.AddWithValue("@id", person.Id);
                sqlCmd.Parameters.AddWithValue("@firstname", person.Firstname);
                sqlCmd.Parameters.AddWithValue("@lastname", person.Lastname);
                sqlCmd.Parameters.AddWithValue("@department", person.Department);

                sqlCon.Open();
                int rowInserted = sqlCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {

                sqlCon.Close();
            }
        }


        [HttpPut]

        public void Update(int id, Data dep)
        {
            try
            {
                sqlCon = Connection();
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = $"update table2 set department=@department where id={id}";
                sqlCmd.Connection = sqlCon;
                sqlCmd.Parameters.AddWithValue("@department", dep.Department);


                sqlCon.Open();
                int rowUpdated = sqlCmd.ExecuteNonQuery();
                sqlCon.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                sqlCon.Close();
            }
            }


        // Delete() CRUD Operation

        [ActionName("DeleteEmployee")]
        public void DeleteEmployeeByID(int id)
        {
            SqlCommand sqlCmd = new SqlCommand();

            try
            {
                sqlCon = Connection();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "delete from table2 where Id=" + id + "";
                sqlCmd.Connection = sqlCon;
                sqlCon.Open();
                int rowDeleted = sqlCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            finally
            {
                sqlCon.Close();
            }
        }

    }
}