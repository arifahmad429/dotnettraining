﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTrainingCodes
{
    class Explicit
    {
        static void Main(string[] args)
        {
            int? num = null;

            if(num == null)
            {
                Console.WriteLine("NULL");
            }
            else
            {
                Console.WriteLine(num);     // if this line would execute, then it will not display any null value 
            }

            Console.ReadKey();
        }
    }
}

