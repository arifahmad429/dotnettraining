﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PartyPlanner
{
    public class Party
    {
        private string _venueType;
        private string _partyShift;
        private int _guests;
        private int _decoration;
        private int _catering;
        private int _basicCost;
        //Venue type can be either "Open Farm House" or "Packed Banquet".
        public string VenueType
        {

            get { return _venueType; }
            set
            {
                _venueType = value;
            }
        }
        //Party shift can be either "Morning" or "Evening".
        public string PartyShift
        {
            get { return _partyShift; }
            set
            {
                _partyShift = value;
            }
        }
        //Number of Guests
        public int Guests
        {
            get { return _guests; }
            set
            {
                _guests = value;
            }
        }
        //Decoration type can be either "Balloon Decor" or "Flower Decor".
        public int Decoration
        {
            get { return _decoration; }
            set
            {
                _decoration = value;
            }
        }
        //Catering Type refers the packages of catering which
        //contains "Regular", "Luxury" and "Delux" packages.
        public int Catering
        {
            get { return _catering; }
            set
            {
                _catering = value;
            }
        }
        public int BasicCost
        {
            get { return _basicCost; }
            set
            {
                _basicCost = value;
            }
        }
    }
}
