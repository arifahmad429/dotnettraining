﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceTask
{
    class Month_Enum
    {
        enum Month
        {
            January,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public void MonthDisplay()
        {

            Dictionary<Month, int> monthDays = new Dictionary<Month, int>();

            monthDays.Add(Month.January, 31);
            monthDays.Add(Month.February, 28);
            monthDays.Add(Month.March, 31);
            monthDays.Add(Month.April, 30);
            monthDays.Add(Month.May, 31);
            monthDays.Add(Month.June, 30);
            monthDays.Add(Month.July, 31);
            monthDays.Add(Month.August, 30);
            monthDays.Add(Month.September, 31);
            monthDays.Add(Month.October, 30);
            monthDays.Add(Month.November, 31);
            monthDays.Add(Month.December, 30);


            string x;

            Console.WriteLine("Enter a month");
            x = Console.ReadLine();
            foreach(KeyValuePair<Month, int> ele in monthDays)
            {
                if (ele.Key.ToString() == x)
                {
                    Console.WriteLine(ele.Value);
                }
                else
                    continue;
            }

            Console.ReadKey();
        }
        
       
        
    }
}
