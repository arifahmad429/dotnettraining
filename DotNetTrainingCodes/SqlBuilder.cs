﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;



namespace SqlConnections
{
    class SqlBuilder
    {
        public static void Main(string[] args)
        {
            string ConnectionString = "Data source=LAPR259; " +
               "initial catalog=master; integrated security= true;";

            SqlConnection conn = new SqlConnection(ConnectionString);

            // open the connection  
            conn.Open();

            // Create a data adapter object  
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM person CompanyClient by ClientName", conn);

            // Ceate a command builder object  
            SqlCommandBuilder builder = new SqlCommandBuilder(adapter);

            // Create a data Set object  

            DataSet ds = new DataSet("PersonSet");
            adapter.Fill(ds, "Persons");

            // Create a data table object and add a new row  

            DataTable PersonTable = ds.Tables["Persons"];
            DataRow row = PersonTable.NewRow();
            row["person_id"] = "133";
            row["person_name"] = "DangerField";
            row["DOB"] = "17/11/1999";
            PersonTable.Rows.Add(row);

            // update data adapter  
            adapter.Update(ds, "Persons");

            MessageBox.Show(row["person_id"].ToString().Trim() + " " + row["person_name"].ToString().Trim() + " Added to Persons");
        }
           catch (Exception e)
           {
               Console.WriteLine(e);
           }
    Console.ReadKey();
        }
