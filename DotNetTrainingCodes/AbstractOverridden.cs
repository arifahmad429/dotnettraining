﻿using System;

namespace AbstractOverridden
{
    public abstract class Vehicle
    {
        string vehicleName;
        string vehicleColor;
        public int vehicleSpeed;
        int vehicleWheels;
        public readonly int MaxSpeed = 264;

        public Vehicle(string color, int noOfWheels, int maxSpeed)
        {
            vehicleColor = color;
            vehicleWheels = noOfWheels;
            maxSpeed = MaxSpeed;

        }

       

        public string VehicleColor
        {
            get
            {
                return vehicleColor;
            }
            set
            {
                vehicleColor = value;
            }
        }


        public int NoOfWheels
        {
            get
            {
                return vehicleWheels;
            }
            set
            {
                vehicleWheels = value;
            }
        }

        public int VehicleSpeed
        {
            get
            {
                return vehicleSpeed;
            }
            set
            {
                vehicleSpeed = value;
            }
        }

        public string VehicleName
        {
            get
            {
                return vehicleName;
            }
            set
            {
                vehicleName = value;
            }
        }

        public abstract void CaclulateTollAmount();

    }
    sealed class Car : Vehicle
    {

        public Car(string vehicleColor, int noOfWheels, int vehicleSpeed) : base("Brown", 4, 132)
        {

            Console.WriteLine(" Ferrari of {0} having {1} wheels running at speed {2} Km/hr", vehicleColor, noOfWheels, vehicleSpeed);
        }

        public override void CaclulateTollAmount()
        {
            Console.WriteLine("  The Toll amount of CAR is Rs.150/-\n\n");
        }
    }

    sealed class Bike : Vehicle
    {

        public Bike(string vehicleColor, int noOfWheels, int vehicleSpeed) : base("Black", 3, 85)
        {
            Console.WriteLine(" Yamaha R-15 of {0} having {1} wheels running at speed {2} Km/hr", VehicleColor, noOfWheels, vehicleSpeed);
        }

        public override void CaclulateTollAmount()
        {
            Console.WriteLine("  The Toll amount of BIKE is Rs.80/-\n\n");
        }


    }


        public class Program
        {
            public static void Main(string[] args)
            {

                Console.WriteLine(" Press a button to Start the CAR..!!\n");
                Console.ReadKey();

                Vehicle c, b;
                c = new Car("Red", 4, 210);
                c.CaclulateTollAmount();

                Console.ReadKey();

                Console.WriteLine(" Press a button to Start the BIKE..!!\n");
                Console.ReadKey();
             
                 b = new Bike("Red", 2, 94);
                 b.CaclulateTollAmount();
            Console.ReadKey();
                
               
            }
        }
    }


    