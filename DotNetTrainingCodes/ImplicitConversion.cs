﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTrainingCodes
{
    class ImplicitConversion
    {
        static void main(string[] args)
        {
            int myInt = 9;
            double myDouble = myInt;
            Console.WriteLine(myInt);
            Console.WriteLine(myDouble);
            Console.ReadKey();
        }
    }
}
