﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DotNetTrainingCodes
{
    class ObservableCollection
    {
        public static void Main(string[] args)
        {
            List<int> list = new List<int>();

            list.Add(34);
            list.Add(78);
            list.Add(12);
            list.Add(76);
            list.Add(452);
            list.Add(11);
            list.Add(69);

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }

            ObservableCollection<int> oc1 = new ObservableCollection<int>(list);
            ObservableCollection<int> oc2 = oc1;

            oc1.Add(8564);

            Console.WriteLine("The list after modification are:\n");
            for (int i = 0; i < oc2.Count; i++)
            {
                Console.WriteLine(oc2[i]);
            }
        }
    }
}
