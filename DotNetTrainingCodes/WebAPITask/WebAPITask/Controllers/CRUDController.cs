﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITask.Models;

namespace WebAPITask.Controllers
{
    public class CRUDController : ApiController

    {
        //GET() CRUD Operation

        [HttpGet]
        [ActionName("GetEmployeeByID")]
        public Data Get()
        {
            SqlDataReader reader = null;
            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = @"data source = LAPR259; initial catalog = master; integrated security = true";

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select * from table2 ";
            sqlCmd.Connection = sqlCon;
            sqlCon.Open();
            reader = sqlCmd.ExecuteReader();
            Data detail = null;

            if (reader.Read())
            {
                detail = new Data();
                detail.Firstname = reader.GetValue(0).ToString();
                detail.Lastname = reader.GetValue(1).ToString();
                detail.Department = reader.GetValue(2).ToString();
                detail.Id = Convert.ToInt32(reader.GetValue(3));

            }
            sqlCon.Close();
            return detail;

        }

        //Post() operation
        
        [HttpPost]
        public string AddEmployee(Data employee)
        {

            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = @"data source = LAPR259; initial catalog = master; integrated security = true;";
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandType = CommandType.Text;
            sqlcmd.CommandText = "insert into document (firstname,lastname, department, id) values (@firstname,@lastname, @department, @id)";
            sqlcmd.Connection = sqlCon;


            sqlcmd.Parameters.AddWithValue("@firstname", employee.Firstname);
            sqlcmd.Parameters.AddWithValue("@lastname", employee.Lastname);
            sqlcmd.Parameters.AddWithValue("@department", employee.Department);
            sqlcmd.Parameters.AddWithValue("@id", employee.Id);
            sqlCon.Open();
            int rowinserted = sqlcmd.ExecuteNonQuery();

            sqlCon.Close();
            return "executed";
        }

        // Delete() CRUD Operation

        [ActionName("DeleteEmployee")]
        public void DeleteEmployeeByID(int id)
        {
            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = @"data source = LAPR259; initial catalog = master; integrated security = true;";

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "delete from table2 where Id=" + id + "";
            sqlCmd.Connection = sqlCon;
            sqlCon.Open();
            int rowDeleted = sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
        }

    }
}

