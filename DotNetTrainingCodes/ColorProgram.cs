﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNETTraining
{
    class ColorProgram
    {
        static void Main(string[] args)
        {
            // Create a method that will take an integer as a nullable parameter.
            // Return "true" if the passed integer has some value in it and is not null
            // Return "false" otherwise.

            Console.Write("Enter the Color: ");
            String inputColor = Console.ReadLine();

            switch (inputColor)
            {

                case "Red":
                    Console.WriteLine("It is RED");
                    break;

                case "red":
                    Console.WriteLine("It is RED");
                    break;

                case "Blue":
                    Console.WriteLine("It is BLUE");
                    break;


                case "blue":
                    Console.WriteLine("It is BLUE");
                    break;

                case "Green":
                    Console.WriteLine("It is GREEN");
                    break;

                case "green":
                    Console.WriteLine("It is GREEN");
                    break;

                default:
                    Console.WriteLine("Switch to any other Color type");
                    break;
            }


            Console.ReadKey();
        }
    }
}