
// Implementation of JQuery Task: 25th March, 2020

$(document).ready(function(){$("#name").keyup(function(){
	var name=$("#name").val();
	if(name.length==0)
	{
		$("#nameCheck").show();
		$("#nameCheck").text("Input field must required");
		$("#nameCheck").css("color","red");
	}
	 else if(name.length>100)
	{
		$("#nameCheck").show();
		$("#nameCheck").text("Input field must required");
		$("#nameCheck").css("color","red");
	}
	else
	{
		$("#nameCheck").hide();
	}
	}),
	$("#birthday").keyup(function(){
	var date = $("#birthday").val();
	if(date=="")
	{
		$("#birthdayCheck").show();
		$("#birthdayCheck").text("Input field must required");
		$("#birthdayCheck").css("color","red");
	}
	else
	{
		$("#birthdayCheck").hide();
	}
	}),
	$("#phone").keyup(function(){
		var telephone=$("#phone").val();
	if(telephone.length==0)
	{
		$("#phoneCheck").show();
		$("#phoneCheck").text("Input field must required");
		$("#phoneCheck").style.color="Red";
	}
	else if(isNaN(telephone))
	{
		$("#phoneCheck").show();
		$("#phoneCheck").text("Please insert the numberic input");
		$("#phoneCheck").css("color","red");
	}
	else if(telephone.length!=10)
	{
		$("#phoneCheck").show();
		$("#phoneCheck").text("Phone number should be 10 digits");
		$("#phoneCheck").css("color","red");
	}
	else
	{
		$("#phoneCheck").hide();
	}
	}),
	$("#email").keyup(function(){
	var email=$("#email").val();
	 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(email.length==0)
	{
		$("#emailCheck").show();
		$("#emailCheck").text("Input field must required");
		$("#emailCheck").css("color","red");	
	}
	else if(!regex.test(email))
	{
		$("#emailCheck").show();
		$("#emailCheck").text("Email must contain '@' and '.com'");
	}
	else
	{
		$("#emailCheck").hide();
	}
	}),
	$("#department").change(function(){
	var department_name=$("#department").val();
	if (department_name == "Select the department")
	{
		$("#departmentCheck").show();
		$("#departmentCheck").txt("Please Select the department");
		$("#departmentCheck").css("color","red");
	}
	else
	{
		$("#departmentCheck").hide();
	}
	}),
	
	$("#state").change(function(){
	var state_var=$("#state").val();
	if (state_var == "Select the state")
	{
		$("#stateCheck").show();
		$("#stateCheck").txt("Please Select the state");
		$("#stateCheck").css("color","red");
	}
	else
	{
		$("#state").hide();
	}
	}),
	$("#zip").keyup(function(){
	var address=$("#address").val();
	if(address.length==0)
	{
		$("#phoneCheck").text("Please Provide the input");
		$("#phoneCheck").style.color="Red";
	}
	else
	{
		$("#phoneCheck").hide();
	}
	}),
	$("#zip").keyup(function(){
		var zip=$("#zip").val();
	if(zip.length==0)
	{
		$("#zipCheck").show();
		$("#zipCheck").text("Please Provide the input");
		$("#zipCheck").style.color="Red";
	}
	else if(isNaN(zip))
	{
		$("#zipCheck").show();
		$("#zipCheck").text("Please provide the numberic input");
		$("#zipCheck").css("color","red");
	}
	else if(zip.length!=6)
	{
		$("#zipCheck").show();
		$("#zipCheck").text("Zip should be of size 6");
		$("#zipCheck").css("color","red");
	}
	else
	{
		$("#zipCheck").hide();
	}
	});
});

//select State
var citiesByState = {
Uttar Pradesh = ["Select","Noida","Greater Noida","Allahabad","Lucknow","Mirzapur"];
Madhya Pradesh = ["Select","Bhopal","Jabalpur","Rewa","Shahdol","LalGanj"];
Maharashtra = ["Select","Mumbai","Pune","Nagpur","Aurangabad","Nashik"];}
function makeSubmenu(value) 
{
	if(value.length==0) 
	{	
		document.getElementById("city").innerHTML = "<option></option>";
	}
	else 
	{
		var citiesOptions = "";
		for(cityId in citiesByState[value]) 
		{
			citiesOptions+="<option>"+citiesByState[value][cityId]+"</option>";
		}
		document.getElementById("city").innerHTML = citiesOptions;
	}
}

function resetSelection() {
document.getElementById("state").selectedIndex = 0;
document.getElementById("city").selectedIndex = 0;
}





// Java Script Implementation


// function SubmitFunction() 
// {	

// var name= document.getElementById("fname").value;
// var check=true;

// if(name=="")
// {
  // document.getElementById("nameCheck").innerHTML = "Name must be filled";
  // document.getElementById("nameCheck").style.color="red";
	// check=false;
// }

// if(name.length > 100)
// {
	// document.getElementById("nameCheck").innerHTML = "Name should be less than 100 characters";
    // document.getElementById("nameCheck").style.color="red";
		// check = false;
// }

	// var dob= document.getElementById("birthday").value;

// if(dob=="")
// {
  // document.getElementById("birthdayCheck").innerHTML = "DateOfBirth must be filled";
  // document.getElementById("birthdayCheck").style.color="red";
	// check=false;
// }


// var contact= document.getElementById("phone").value;

// if(contact.length < 1 || contact.length > 10)
// {
	// document.getElementById("phoneCheck").innerHTML="Phone number must be 10 digits";
    // document.getElementById("phoneCheck").style.color="red";
		// check=false;
// }

// if(contact == "")
// {
	// document.getElementById("phoneCheck").innerHTML="Phone field must be filled";
	// document.getElementById("phoneCheck").style.color="red";
		// check=false;

// }
	
	// var email= document.getElementById("email").value;
	// var atindex=email.indexOf("@");
	// var dotindex=email.lastIndexOf(".com");  

// if(email=="")
// {
  // document.getElementById("emailCheck").innerHTML = "Email must be filled";
  // document.getElementById("emailCheck").style.color="red";
		// check=false;
// }

// if (atindex<1 || dotindex<atindex+2 || dotindex+2>=email.length)
// {  
	// document.getElementById("emailCheck").innerHTML="Enter a valid email address.";
	// document.getElementById("emailCheck").style.color="Red";
	// check= false;
// } 

// if(email.length > 256)
// {
	// document.getElementById("emailCheck").innerHTML = "Email should be less than 256 characters";
	// document.getElementById("emailCheck").style.color="red";
		// check=false;
// }

// var department=document.getElementById("department").value;

// if(department=="Select")
// {
	// document.getElementById("departmentCheck").innerHTML="Department must be filled";
	// document.getElementById("departmentCheck").style.color="red";
	// check=false;
// }

// var zipcode= document.getElementById("zip").value;


// if(zipcode.length < 6 || zipcode.length > 6)
// {
	// document.getElementById("zipCheck").innerHTML="ZipCode must be 6 digits";
    // document.getElementById("zipCheck").style.color="red";
	// check=false;
// }

// if(check==false)
// {
	// return false;
// }
// else
// {
	// return true;
// }

// }

// function PhoneValidation(){
// var contact= document.getElementById("phone").value;
  // if(isNaN(contact))
    // {
       // document.getElementById("phoneCheck").innerHTML ="Not valid";
// document.getElementById("phoneCheck").style.color="red";
// }
  // else
// {

	// document.getElementById("phoneCheck").innerHTML ="";
	// phone.toString();
	// if(contact.length==10)
 // {
	// document.getElementById("phoneCheck").innerHTML ="";
 // }
 
 // else
 // {
	// document.getElementById("phoneCheck").innerHTML ="Phone must be of 10 digits";
	// document.getElementById("phoneCheck").style.color="red";
 // }
// }

// }


// function Validation()
// {
// var name= document.getElementById("fname").value;

// if(name!="")
// {
 // document.getElementById("nameCheck").innerHTML ="";
// }


// var dob= document.getElementById("birthday").value;

// if(dob!="")
// {
 // document.getElementById("birthdayCheck").innerHTML ="";
// }



// var email= document.getElementById("email").value;

// if(email!="")
// {
 // document.getElementById("emailCheck").innerHTML ="";
// }

// var dept=document.getElementById("department").value;

// if(dept == "Select")
// {
	// document.getElementById("departmentCheck").innerHTML="Please select Department";
	// document.getElementById("departmentCheck").style.color="red";
// }


// var zip= document.getElementById("zip").value;

// if(zip!="")
// {
 // document.getElementById("zipCheck").innerHTML ="";
 // return true;
// }
// }



// function Cities(){




// var selectState = document.getElementById("state").value;
// var cityList = document.getElementById("city");
// var up = ["Select","Noida","Greater Noida","Allahabad","Lucknow","Mirzapur"];
// var mp = ["Select","Bhopal","Jabalpur","Rewa","Shahdol","LalGanj"];
// var mh = ["Select","Mumbai","Pune","Nagpur","Aurangabad","Nashik"];

// var i=0;



// if(selectState == "Uttar Pradesh")
// {
	// for(i=0; i<6; i++)
	// {
		// cityList[i].innerHTML = up[i];
	// }	
// }

// if(selectState == "Madhya Pradesh")
// {
	// for(i=0; i<6; i++)
	// {
		// cityList[i].innerHTML = mp[i];
	// }	
// }

// if(selectState == "Maharashtra")
// {
	// for(i=0; i<6; i++)
	// {
		// cityList[i].innerHTML = mh[i];
	// }	
// }

// }
