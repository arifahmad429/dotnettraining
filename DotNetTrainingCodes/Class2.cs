﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTrainingCodes
{
    class Class2
    {
        static void Main(string[] args)
        {
            int myInt = 10;
            double myDouble = 76.32;
            bool myBool = true;

            Console.WriteLine(Convert.ToString(myInt));    
            Console.WriteLine(Convert.ToDouble(myInt));    
            Console.WriteLine(Convert.ToInt32(myDouble));  
            Console.WriteLine(Convert.ToString(myBool)); 
            Console.ReadKey();
        }
    }
}
