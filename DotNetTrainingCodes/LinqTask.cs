﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace LinqTraining
{  
    class Student
    {
        public int StudentID { get; set; }
        public String StudentName { get; set; }
        public int Age { get; set; }
    }
    class Program
    {

        public int StudentID;
        static void Main(string[] args)
        {


            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPR259; Initial Catalog=master; Integrated Security = True;");

            List<Student> info = new List<Student>()
            {
                new Student(){StudentID = 1, StudentName = "Arif Ahmad", Age = 22},
                new Student(){StudentID = 2, StudentName = "Prabhat Kumar", Age = 21},
                new Student(){StudentID = 3, StudentName = "Ayush Aggarwal", Age = 21},
                new Student(){StudentID = 8, StudentName = "Aastha Jain", Age = 28},


            };


            var linqQuery = (from x in info where x.Age > 21 select x.StudentName).ToList();


            Console.WriteLine("\n\n Linq using WHERE Query:\n");

            foreach (var i in linqQuery)
            {
                Console.WriteLine(i);
            }



            Console.WriteLine("\n\n List using Order by:\n");
            
            var studentsInAscOrder = info.OrderBy(s => s.Age).ToList();

            foreach (var j in studentsInAscOrder)
            {
                Console.WriteLine(j.StudentName);
            }



            List<string> Teacher = new List<string>();
            Teacher.Add("Ayush Aggarwal");
            Teacher.Add("Prabhat Kumar");
            Teacher.Add("Anand Srivastava");
            Teacher.Add("Tarun Singh");
            


            var innerJoin = info.Join(Teacher,
                      str1 => str1.StudentName,
                      str2 => str2,
                      (str1, str2) => str1);


            Console.WriteLine("\n\n List using Inner Join by:\n");

            foreach (var x in innerJoin)
            {
                Console.WriteLine(x.StudentID +" "+x.StudentName);
            }



            List<string> namesTask = new List<string>();

            namesTask.Add("Prabhat");
            namesTask.Add("Anand");
            namesTask.Add("Arif");


            string checkName;

            Console.WriteLine("Please Enter Name:\n");

            checkName = Console.ReadLine();

            var check = (from x in namesTask where x.Contains(checkName) select x).ToList();
            
            int y = check.Count();
            
                        if (y > 0)
                            {
                checkName = checkName + "_" + Convert.ToString(y);
                
                            }

            namesTask.Add(checkName);
            
            
            Console.WriteLine("\nanswer is :\n");
            
            namesTask.OrderBy(x => x);
            
                        foreach (var na in namesTask)
                            {
                Console.WriteLine(na);
                            }
            



                        Console.ReadKey();
        }
    }
}
