﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceTask
{
    class Person
    {
        private string _name;
        private int _age;


        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }



        public void Display()
        {

            List<Person> personDetail = new List<Person>();


            Console.WriteLine("Enter 5 number of people with age:\n");

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("\nEnter Name:");
                Name = Console.ReadLine();

                Console.WriteLine("Enter Age");
                Age = Convert.ToInt32(Console.ReadLine());
                personDetail.Add(new Person { Name = Name, Age = Age });

            }

            Console.WriteLine("\n\nThe person above from age of 60 are:");

            for (int i = 0; i < 5; i++)
            {   
                if (personDetail[i].Age > 60)
                {   
                    Console.WriteLine(personDetail[i].Name, personDetail[i].Age);
                }
            }
        }
    }
}