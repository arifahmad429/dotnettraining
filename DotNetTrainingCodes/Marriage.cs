﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PartyPlanner
{
    public class Marriage : Party
    {

        public Marriage()
        {
            string select;
            string selectVenue;
            int totalCharges;
            int serviceCharges;
            Console.WriteLine("\nAvailable Venue Types are: \n1. Open Farm House \n2. Packed Banquet");
            Console.WriteLine("Please select the Venue type from above two options[1 or 2]");
            selectVenue = Console.ReadLine();
            int.TryParse(selectVenue, out int a);
            while (a != 1 && a != 2)
            {
                Console.WriteLine("Please Enter the available Venue Type");
                selectVenue = Console.ReadLine();
                int.TryParse(selectVenue, out a);
            }


            //
            Console.WriteLine("\nEnter the Number of Guests that will come in your party:" +
                "\n[Minimun guests should be 100 and maximum can be 1000]");
            string Guests = Console.ReadLine();
            int.TryParse(Guests, out int guest);
            while (guest < 100 || guest > 1000)
            {
                Console.WriteLine("\nEnter the Number of Guests that will come in your party:" +
                "\n[Minimun guests should be 100 and maximum can be 1000]");
                Guests = Console.ReadLine();
                int.TryParse(Guests, out guest);
            }

            Console.WriteLine("\nCatering Packages Available: \n1. Regular \n2. Luxury \n3. Delux");
            Console.WriteLine("Select the Catering package[1 or 2 or 3]\n");
            select = Console.ReadLine();
            int.TryParse(select, out int b);
            while (b != 1 && b != 2 && b != 3)
            {
                Console.WriteLine("\nPlease select from the available Package");
                Console.WriteLine("\nCatering Packages Available: \n1. Regular \n2. Luxury \n3. Delux");
                Console.WriteLine("Select the Catering package[1 or 2 or 3]");
                select = Console.ReadLine();
                int.TryParse(select, out b);
            }


            switch (b)
            {
                case 1:
                    Console.WriteLine("\nYou have selected the regular Package");
                    //select1 = "Regular";
                    BasicCost = 65000;
                    Console.WriteLine("Fixed base cost: Rs{0}", BasicCost);
                    Catering = 1150 * guest;
                    Console.WriteLine("Catering charges, Rs 1150 per head: {0}", Catering);
                    Decoration = 3 * guest;
                    Console.WriteLine("Decoration charges, Rs 3 per head: {0}", Decoration);

                    break;
                case 2:
                    Console.WriteLine("\nYou have selected the luxury Package");
                    BasicCost = 80000;
                    //select1 = "Luxury";
                    Console.WriteLine("Fixed base cost: Rs{0}", BasicCost);
                    Catering = 1300 * guest;
                    Console.WriteLine("Catering rates, Rs 1300 per head: Rs{0}", Catering);
                    Decoration = 5 * guest;
                    Console.WriteLine("Decoration charges, Rs 5 per head: Rs{0}", Decoration);

                    break;
                case 3:
                    Console.WriteLine("\nYou have selected the Delux Package");
                    BasicCost = 105000;
                    //select1 = "Delux";
                    Console.WriteLine("Fixed base cost: Rs{0}", BasicCost);
                    Catering = 1500 * guest;
                    Console.WriteLine("Catering rates, Rs 1500 per head: {0}", Catering);
                    Decoration = 7 * guest;
                    Console.WriteLine("Decoration charges, Rs 7 per head: {0}", Decoration);
                    break;

            }
            Console.WriteLine("\n\t::::::::::Are You Confirming Your Booking::::::::::" +
                "\n\t\t(Y for Yes, N for No)");
            string confirmation = Console.ReadLine();
            while (confirmation != "y" && confirmation != "Y" && confirmation != "n" && confirmation != "N")
            {
                Console.WriteLine("\nWrong Input\n" +
                    "Select Either Y or N ");
                Console.WriteLine("Are You Confirming Your Booking: (Y for Yes, N for No)");
                confirmation = Console.ReadLine();
            }
            if (confirmation == "y" || confirmation == "Y")
            {

                serviceCharges = 3 * guest;
                totalCharges = Decoration + Catering + serviceCharges + BasicCost;




                StreamWriter sWriter = new StreamWriter("C:/Users/arifa/Desktop/Receipt.txt");
                sWriter.WriteLine("\n\t::::::::::Booking Details:::::::::");
                sWriter.WriteLine("\t::::::::::::::Receipt:::::::::::::\n ");
                sWriter.WriteLine("\tParty\t\t\t\t\t: Marriage");
                //Console.WriteLine("\tVenue Type\t\t\t\t: {0}",selectVenue);
                sWriter.WriteLine("\tNumber of Guests\t\t\t: {0}", guest);
                //Console.WriteLine("\tPackage Type\t\t\t\t: {0}", select1);
                sWriter.WriteLine("\tFixed Base Cost of your Package\t\t: Rs {0}", BasicCost);
                sWriter.WriteLine("\tCatering Charges\t\t\t: Rs {0}", Catering);
                sWriter.WriteLine("\tDecoration Charges\t\t\t: Rs {0}", Decoration);
                sWriter.WriteLine("\tService Charges\t\t\t\t: Rs {0}", serviceCharges);
                sWriter.WriteLine("--------------------------------------------------------------------------");
                sWriter.WriteLine("\tTotal Expenditure\t\t\t: Rs {0}", totalCharges);
                sWriter.Close();


                Console.WriteLine("\n\t::::::::::Booking Details:::::::::");
                Console.WriteLine("\t::::::::::::::Receipt:::::::::::::\n ");
                Console.WriteLine("\tParty\t\t\t\t\t: Marriage");
                //Console.WriteLine("\tVenue Type\t\t\t\t: {0}",selectVenue);
                Console.WriteLine("\tNumber of Guests\t\t\t: {0}", guest);
                //Console.WriteLine("\tPackage Type\t\t\t\t: {0}", select1);
                Console.WriteLine("\tFixed Base Cost of your Package\t\t: Rs {0}", BasicCost);
                Console.WriteLine("\tCatering Charges\t\t\t: Rs {0}", Catering);
                Console.WriteLine("\tDecoration Charges\t\t\t: Rs {0}", Decoration);
                Console.WriteLine("\tService Charges\t\t\t\t: Rs {0}", serviceCharges);
                Console.WriteLine("--------------------------------------------------------------------------");
                Console.WriteLine("\tTotal Expenditure\t\t\t: Rs {0}", totalCharges);
            }
            else
            {
                Console.WriteLine("Do you want to book your party again ?\n (Y for Yes, N for No)");
                string again = Console.ReadLine();
                while (again != "y" && again != "Y" && again != "n" && again != "N")
                {
                    Console.WriteLine("Wrong Input\n" +
                    "Select Either Y or N ");
                    Console.WriteLine("Do you want to book your party again ?\n (Y for Yes, N for No)");
                    again = Console.ReadLine();
                }
                if (again == "y" || again == "Y")
                {
                    Console.WriteLine("Getting you to Homepage:");

                }
                if (again == "n" || again == "N")
                {
                    Console.WriteLine("Thank You! Visit Again.");
                }
            }


        }
    }
}