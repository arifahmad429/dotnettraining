﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;


namespace WebAPIDemo
{
    public class DatabaseCrudController : ApiController
    {
        static void Main(string[] args)
        {
          SqlConnection con = new SqlConnection("Data source=LAPR259; " +
               "initial catalog=master; integrated security= true;");

            con.Open();
            string id = "", PersonName = "", birthday = "";

            SqlCommand sqldDataAdapter = new SqlCommand("select * from Persons", con);

            SqlDataReader sqlDataReader = sqldDataAdapter.ExecuteReader();

            if (sqlDataReader.Read())
            {
                id = sqlDataReader["person_id"].ToString();
                PersonName = sqlDataReader["person_name"].ToString();
                birthday = sqlDataReader["DOB"].ToString();
            }

            Console.WriteLine(id);
            Console.ReadKey();
        }
        
    }
}